CoSApp documentation
====================

.. only:: html

   :Release: |release|
   :Date: |today|

.. toctree::
   :hidden:
   :caption: License

   license

.. include:: readme.rst

.. toctree::
   :maxdepth: 2
   :caption: Getting started

   installation
   tutorials/00-Introduction

.. toctree::
   :maxdepth: 1

   authors
   history

.. toctree::
   :maxdepth: 2
   :caption: CoSApp developer guide
   
   contributing
   developer/scenarii
   developer/logger
   developer/project_structure
   developer/installation_cases
   source/modules


Indices and tables
------------------

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

